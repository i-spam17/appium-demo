package tests.swipe;

import base.BaseTest;
import jdk.jfr.Description;
import org.testng.annotations.Test;

public class PositiveTest extends BaseTest {
    @Test
    @Description("Проверяем заголовок третьей карточки в карусели")
    public void checkTitleTextThirdCard() {
        openApp("pixel_3")
                .tapSwipe()
                .numberSwipeToTheLeft(driver, 2)
                .checkTextTitleCard();
    }

    @Test
    @Description("Проверяем заголовок третьей карточки в карусели")
    public void checkTitleTextThirdCard2() {
        openApp("pixel_2")
                .tapSwipe()
                .tapThirdDotUnderCarousel()
                .checkTextTitleCard();
    }
}
