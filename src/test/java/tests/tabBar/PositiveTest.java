package tests.tabBar;

import base.BaseTest;
import jdk.jfr.Description;
import org.testng.annotations.Test;

public class PositiveTest extends BaseTest {
    @Test
    @Description("Проверяем изменение цвета выбранной иконки Home")
    public void checkChangeIconHomeWhenTap() {
        openApp("pixel_3")
                .tapHome()
                .checkIconHomeScreenshot();
    }

    @Test
    @Description("Проверяем изменение цвета выбранной иконки Forms")
    public void checkChangeIconFormsWhenTap() {
        openApp("pixel_3")
                .tapForms()
                .checkFormsActivityExist()
                .checkIconFormsScreenshot();
    }

    @Test
    @Description("Проверяем изменение цвета выбранной иконки Swipe")
    public void checkChangeIconSwipeWhenTap() {
        openApp("pixel_3")
                .tapSwipe()
                .checkSwipeActivityExist()
                .checkIconSwipeScreenshot();
    }

    @Test
    @Description("Проверяем изменение цвета выбранной иконки WebView")
    public void checkChangeIconWebViewWhenTap() {
        openApp("pixel_3")
                .tapWebView()
                .checkWebViewActivityExist()
                .checkIconWebViewScreenshot();
    }

    @Test
    @Description("Проверяем изменение цвета выбранной иконки Login")
    public void checkChangeIconLoginWhenTap() {
        openApp("pixel_3")
                .tapLogin()
                .checkLoginActivityScreenExist()
                .checkIconLoginScreenshot();
    }
}
