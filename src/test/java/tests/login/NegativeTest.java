package tests.login;

import base.BaseTest;
import io.qameta.allure.Description;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class NegativeTest extends BaseTest {
    private static final String EMAIL_ERROR_TEXT = "Please enter a valid email address";
    private static final String PASSWORD_ERROR_TEXT = "Please enter at least 8 characters";

    @DataProvider
    public Object[][] negativeData() {
        return new Object[][]{
                {" ", " "},
                {"12345678", ""},
                {"qwe.qwe", "0"},
                {"q we@qwe.qwe", "1234567"},
                {"qwe qwe.qwe", "abcdefg"},
                {"qwe@qwe", "йййййййй"},
                {"#/_!@\\'*&?", "#/_!@\\'*&?"},
                {" @ . ", "ФЫADFDFF"},
                {"-1", "-1"},
                {"12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"}
        };
    }

    @Test(dataProvider = "negativeData")
    @Description("Проверяем сообщение об ошибке при вводе невалидного email & password")
    public void checkFieldEmail(String login, String password) {
        openApp("pixel_2")
                .tapLogin()
                .fillInputLogin(login)
                .fillInputPassword(password)
                .tapLoginButton()
                .checkEmailFieldErrorText(EMAIL_ERROR_TEXT)
                .checkPasswordFieldErrorText(PASSWORD_ERROR_TEXT);
    }
}
