package locators;

import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.AppiumBy;

import static com.codeborne.selenide.Selenide.$;

public class WebViewActivityLocators {
    public SelenideElement webViewActivityScreen() {
        return $(AppiumBy.accessibilityId("WebdriverIO"));
    }

    public SelenideElement loadingScreen() {
        return $(AppiumBy.id("android:id/content"));
    }
}
