package locators;

import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.AppiumBy;

import static com.codeborne.selenide.Selenide.$;

public class AlertSuccessLocators {
    public SelenideElement alertMsgTitle() {
        return $(AppiumBy.id("android:id/alertTitle"));
    }

    public SelenideElement alertMsg() {
        return $(AppiumBy.id("android:id/message"));
    }

    public SelenideElement alertBtnOK() {
        return $(AppiumBy.id("android:id/button1"));
    }
}
