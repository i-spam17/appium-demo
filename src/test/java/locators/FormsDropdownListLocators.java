package locators;

import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.AppiumBy;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class FormsDropdownListLocators extends FormsLocators {
    public SelenideElement dropdownWindow() {
        return $(AppiumBy.id("android:id/content"));
    }

    public SelenideElement secondElement() {
        //todo first element - fail!
        List<SelenideElement> elements = $$(AppiumBy.id("android:id/text1"));
        return elements.get(1);
    }
}
