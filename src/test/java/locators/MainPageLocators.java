package locators;

import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.AppiumBy;

import static com.codeborne.selenide.Selenide.$;

public class MainPageLocators {
    public SelenideElement homeScreen() {
        return $(AppiumBy.accessibilityId("Home-screen"));
    }
}
