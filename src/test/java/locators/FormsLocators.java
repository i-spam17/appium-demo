package locators;

import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.AppiumBy;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class FormsLocators {
    public SelenideElement formsScreen() {
        return $(AppiumBy.accessibilityId("Forms-screen"));
    }

    public SelenideElement elementSwitch() {
        return $(AppiumBy.accessibilityId("switch"));
    }

    public SelenideElement textUnderSwitch() {
        return $(AppiumBy.accessibilityId("switch-text"));
    }

    public SelenideElement dropdownElement() {
        return $(AppiumBy.accessibilityId("select-Dropdown"));
    }

    public SelenideElement textInDropdownField() {
        List<SelenideElement> elements = $$(AppiumBy.xpath("//*[@content-desc=\"select-Dropdown\"]/*"));
        return elements.get(1);
    }
}
