package pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import locators.FormsDropdownListLocators;

public class FormsDropdownList extends FormsActivity {
    private FormsDropdownListLocators locator() {
        return new FormsDropdownListLocators();
    }

    public static String textDropdownElement;

    @Step("Проверка появления Dropdown списка")
    public FormsDropdownList checkDropdownListExist() {
        locator().dropdownWindow().shouldBe(Condition.exist);
        return this;
    }

    @Step("Тап на элемент webdriver.io is awesome")
    public FormsDropdownList tapFirstElementFromList() {
        textDropdownElement = locator().secondElement().getText();
        locator().secondElement().click();
        return this;
    }

}
