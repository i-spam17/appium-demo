package pages;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import locators.FormsLocators;

import static pages.FormsDropdownList.textDropdownElement;

public class FormsActivity extends TabBar {
    private FormsLocators locator() {
        return new FormsLocators();
    }

    @Step("Проверка загрузки Forms Activity")
    public FormsActivity checkFormsActivityExist() {
        locator().formsScreen().shouldBe(Condition.exist);
        return this;
    }

    @Step("Тап на элемент переключания switch")
    public FormsActivity tapSwitch() {
        locator().elementSwitch().click();
        return this;
    }

    @Step("Проверка элемент switch включен")
    public FormsActivity checkSwitchElementEnabled() {
        locator().textUnderSwitch().shouldHave(Condition.text("Click to turn the switch OFF"));
        return this;
    }

    @Step("Тап на элемент Dropdown")
    public FormsActivity tapDropdownElement() {
        locator().dropdownElement().click();
        return this;
    }

    @Step("Проверка выбранного элемента из списка Dropdown")
    public FormsActivity checkSelectedElementFromDropdownList() {
        locator().textInDropdownField().shouldHave(Condition.text(textDropdownElement));
        return this;
    }

}
