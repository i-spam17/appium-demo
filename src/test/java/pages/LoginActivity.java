package pages;

import com.codeborne.selenide.Condition;
import helpers.Helpers;
import io.qameta.allure.Step;
import locators.LoginPageLocators;

public class LoginActivity extends TabBar {

    private LoginPageLocators locator() {
        return new LoginPageLocators();
    }

    @Step("Заполняем поле \"логин\" в форме")
    public LoginActivity fillInputLogin(String login) {
        locator().inputLogin().sendKeys(login);
        return this;
    }

    @Step("Заполняем поле \"пароль\" в форме")
    public LoginActivity fillInputPassword(String pass) {
        locator().inputPassword().sendKeys(pass);
        return this;
    }

    @Step("Тап по кнопке \"логин\" в форме")
    public LoginActivity tapLoginButton() {
        locator().loginButton().click();
        return this;
    }

    @Step("Проверяем текст ошибки поля Email")
    public LoginActivity checkEmailFieldErrorText(String text) {
        locator().emailErrorText().shouldHave(Condition.text(text));
        return new LoginActivity();
    }

    @Step("Проверяем текст ошибки поля Password")
    public LoginActivity checkPasswordFieldErrorText(String text) {
        locator().passwordErrorText().shouldHave(Condition.text(text));
        return new LoginActivity();
    }

    @Step("Делаем скриншот страницы Login и сравниваем с требованием")
    public LoginActivity checkLoginScreenshot() {
        Helpers.compareScr(locator().loginActivityScreen(), "loginPage.png");
        return this;
    }

    @Step("Проверка загрузки LoginActivity")
    public LoginActivity checkLoginActivityScreenExist() {
        locator().loginActivityScreen().shouldBe(Condition.exist);
        return this;
    }
}
